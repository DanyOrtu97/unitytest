﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiamMovement : MonoBehaviour
{

	private Animator animator;
	private Rigidbody rb;
	public float speed = 1f;
	public float velocity = 0;
	public float turnSpeed = 1f;
	public float turnVelocity = 1f;
	public float jump = 10f;



    // Start is called before the first frame update
    void Start()
    {
		animator = GetComponent<Animator>();
		rb = this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		float y = Input.GetAxis("Vertical");
		float x = Input.GetAxis("Horizontal");
		velocity = y * speed;

		turnVelocity = x * turnSpeed;

		if(velocity != 0)
		{
			animator.SetBool("IsMoving", true);
			animator.SetFloat("Velocity", y);

			rb.velocity = transform.forward * velocity;
		}
		else
		{
			animator.SetBool("IsMoving", false);
		}


		if (Input.GetKeyDown(KeyCode.Space) )
		{
			animator.SetBool("Jump", true);
		}
		else
		{
			animator.SetBool("Jump", false);
		}
		

		Vector3 rotationToApply = new Vector3(0, turnVelocity, 0);

		rb.rotation = Quaternion.Euler(rb.rotation.eulerAngles + rotationToApply);

		animator.SetFloat("TurnRate", x);
    }
}
